#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "plotter.h"
#include "magnifier.h"
#include "smartdialog.h"

#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QMainWindow>
#include <qpixmap.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include <qmap.h>
#include<QMouseEvent>
#include<QDebug>
//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/numeric/ublas/io.hpp>
#include <boost/qvm/mat_operations.hpp>
#include <boost/qvm/mat.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    struct RGB{
        double r;
        double g;
        double b;
    };

private slots:
    void on_actionBilateral_filtering_triggered();
    void on_actionColor_Histogram_Equalization_triggered();
    bool isMousedReleasedOnModifiedImage(QObject *obj, QEvent *event);
    void on_actionSelection_varianta_triggered();
    void on_actionBinarizarea_triggered();
    void on_actionGreyscale_triggered();
    void on_actionSave_as_triggered();
    void on_actionColor_triggered();
    void on_btnSaveAsInitialImage_clicked();
    void on_actionLamba_triggered();
    void on_actionRevert_colors_triggered();
    void on_actionPlot_grey_level_triggered();
    void on_actionLaunch_magnifier_triggered();
    void on_actionGama_triggered();
    void on_actionOglindire_triggered();
    void on_actionHistograma_triggered();
    void on_actionCany_edge_detection_triggered();    
    void on_actionSwap_Images_triggered();
    void on_actionWatershed_triggered();
    void on_actionConex_component_triggered();    
    void on_actionTransformare_proiectiva_triggered();

private:
    void updateVariance();
    void toGreyScale(QImage* image);
    void updateImages(bool isOpen);
    void loadImages();
    bool eventFilter(QObject *obj, QEvent *event) override;
    void launchMagnifierDialog(int x, int y, QImage* image);
    bool isMousedPressedOnInitialImage(QObject *obj, QEvent *event);
    bool isMousedPressedOnModifiedImage(QObject *obj, QEvent *event);
    void plotGreyPixels(int y, QImage* image) const;
    bool isMousedReleasedOnInitialImage(QObject *obj, QEvent *event);
    QString calculateVarianceAndAvg(QImage* image);
    void plotHistogram(int line = -1);
    QImage *gaussian_fiter(double q, QImage *img);
    QImage *canny_edge_detection(double T1, double T2, QImage * img);
    QImage *watershed(QImage *img, int T);

private:
    struct mouse_pos{
        int x;
        int y;
    };
    QMap<QString,double>* transientData;
    QImage* initialImage;
    QString initialImagePath;
    QImage* modifiedImage;
    Magnifier* magnifierDialog;
    Plotter* greyPlotterDialog;
    QMessageBox Msgbox;
    int inPosX;
    int inPosY;
    int outPosX;
    int outPosY;
    bool initialClicked;
    bool modifiedImageLoaded;
    bool isGreyscale;
    QString graphMode;
    bool showGaphTransform = false;
    Ui::MainWindow* ui;
    std::vector<mouse_pos> mouse_clicks;
    std::vector<std::vector<std::vector<double>>> RGBtoHSV(QImage* image, int width, int height);
    QImage *HSVtoRGB(int width, int height, std::vector<std::vector<double> > vecH, std::vector<std::vector<double> > vecS, std::vector<std::vector<double> > vecV);
    QImage *conex_component(QImage *img);
    QImage *transformare_proiectiva(QImage* img);
};

#endif // MAINWINDOW_H
