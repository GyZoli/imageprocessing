 #include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    qApp->installEventFilter(this);
    initialImage = nullptr;
    modifiedImage = nullptr;
    transientData= new QMap<QString,double>();
    magnifierDialog=nullptr;
    greyPlotterDialog=nullptr;
    inPosX = 0;
    inPosY = 0;
    outPosX = 0;
    outPosY = 0;
    initialClicked = true;
    modifiedImageLoaded = false;
    graphMode = "";
    isGreyscale = false;

    ui->setupUi(this);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setValue(0);
}

MainWindow::~MainWindow()
{
    if(initialImage!=nullptr)
    {
        delete initialImage;
        initialImage = nullptr;
    }
    if(modifiedImage != nullptr)
    {
        delete modifiedImage;
        modifiedImage = nullptr;
    }
    if(transientData!= nullptr)
    {
        delete transientData;
        transientData = nullptr;
    }
    if(magnifierDialog!=nullptr)
    {
        delete magnifierDialog;
        magnifierDialog=nullptr;
    }
    if(greyPlotterDialog!=nullptr)
    {
        delete greyPlotterDialog;
        greyPlotterDialog=nullptr;
    }
    delete ui;
}

/*
    Loads an image from disk without modifying its coloration.
*/
void MainWindow::on_actionColor_triggered()
{
    isGreyscale = false;
    loadImages();
    updateImages(true);
}

/*
    Loads an image from disk and transforms it into a greyscaleimage.
*/
void MainWindow::on_actionGreyscale_triggered()
{
    isGreyscale = true;
    loadImages();
    toGreyScale(initialImage);
    updateImages(true);
}

/*
    The SaveAs button saves the modifiedImage on disk on a given path.
*/
void MainWindow::on_actionSave_as_triggered()
{
    QString imagePath = QFileDialog::getSaveFileName(this, tr("Open file"),"",tr("IMAGE (*.jpg *.jpeg *.png *.bmp)"));
    if(imagePath.length()<2)
    {
        return;
    }
    if(modifiedImage != nullptr)
    {
        modifiedImage->save(imagePath);
    }
}

/*
    The method transforms the given image into a greyscale image.
 */
void MainWindow::toGreyScale(QImage* image)
{
    if(image== nullptr)
    {
        return;
    }
    for (int i = 0;  i < image->width();  i++)
    {
        for (int j = 0; j < image->height(); j++)
        {
            int gray = qGray(image->pixel(i,j));
            image->setPixel(i,j, QColor(gray, gray, gray).rgb());
        }
    }
}


/*
    At loading, initialImage and modifiedImage are both initialized with the image taken from the given path.
    But only the initialImage is shown in the UI.
*/
void MainWindow::loadImages()
{
    QString imagePath = QFileDialog::getOpenFileName(this, tr("Open file"),"",tr("IMAGE (*.jpg *.jpeg *.png *.bmp)"));
    if(imagePath.length()<5)
    {
        qDebug("Path too short!");
        return;
    }
    if(initialImage != nullptr)
    {
        delete initialImage;
    }

    //initialImage = new QImage(imagePath);
    //initialImagePath=imagePath;

    QImage *image=new QImage(imagePath);
    int width=image->width();
    int height=image->height();
    int count=image->bitPlaneCount();

    outPosX = width;
    outPosY = height;
    inPosX = 0;
    inPosY = 0;
   // qDebug()<<count;
   if(count==8)
    {

        initialImage=new QImage(width,height,QImage::Format_RGB32);
        for(int i=0;i<width;i++)
        {
            for(int j=0;j<height;j++)
            {
                byte cl=qGray(image->pixel(i,j));
               //QRgb cl=image->pixel(i,j);
               //initialImage->setPixelColor(i,j,cl);
               initialImage->setPixelColor(i,j,qRgb(cl,cl,cl));
            }
        }
    qDebug()<<count;
        delete image;
    }
    else
        initialImage=image;


    if(modifiedImage != nullptr)
    {
        delete modifiedImage;
    }
    //modifiedImage = new QImage(imagePath);
    modifiedImage = new QImage(width,height,QImage::Format_RGB32);
    modifiedImageLoaded = false;
}

/*
    The method updates and shows the initialImage and the modifiedImage. It is supposed to be called after a modification.
    The modifiedImage is shown only if relevant.
 */
void MainWindow::updateImages(bool isOpen)
{
    if(initialImage != nullptr && modifiedImage != nullptr)
    {
        QPixmap pixMapInitial = QPixmap::fromImage(*initialImage);
        QPixmap pixMapModified = QPixmap::fromImage(*modifiedImage);
        QPainter *paint;
        if (initialClicked){
            paint = new QPainter(&pixMapInitial);
        }
        else{
            paint = new QPainter(&pixMapModified);
        }
        if (inPosX !=0 && inPosY !=0){
            paint->setPen(*(new QColor(255,34,255,255)));
            paint->drawRect(inPosX,inPosY,outPosX - inPosX,outPosY - inPosY);
        }

        if(showGaphTransform){
            if(mouse_clicks.size() >= 4){
                QPainterPath path;
                path.moveTo(mouse_clicks[0].x, mouse_clicks[0].y);
                path.lineTo(mouse_clicks[1].x, mouse_clicks[1].y);
                path.lineTo(mouse_clicks[2].x, mouse_clicks[2].y);
                path.lineTo(mouse_clicks[3].x, mouse_clicks[3].y);
                path.lineTo(mouse_clicks[0].x, mouse_clicks[0].y);

                paint->setPen(*(new QColor(255,34,255,255)));
                paint->drawPath(path);
            }
            //showGaphTransform = false;
        }

        ui->label->setPixmap(pixMapInitial);
        if(!isOpen && modifiedImageLoaded) {
            ui->label_2->setPixmap(pixMapModified);
        }
        paint->end();
    }
}

/*
 * The SaveAsInitialImage button replaces the initialImage with the modifiedImage on disk and also on the UI.
 */
void MainWindow::on_btnSaveAsInitialImage_clicked()
{
    if(modifiedImage != nullptr)
    {
        initialImage= new QImage(*modifiedImage);
      //  initialImage->save(initialImagePath);
        updateImages(true);
    }
}

/*
    The lamba property launches a dialog that permits saving into the transientData a needed double value.
*/
void MainWindow::on_actionLamba_triggered()
{
    bool ok;
    SmartDialog lambda("Lambda", &ok);
    //transientData->insert("Lambda",lambda.getValue());
    //Example of usage CTRL+/ to uncomment
    //    if (ok)
    //    {
    //        ui->label->setText(QString::number(transientData->value("Lambda")));
    //    }

}

/*
 * The method manages the behavior when the mouse is pressed.
 * If the mouse is pressed over the initialImage or the modifiedImage, specific algorithms (like zoom in, grey plotter etc.) are triggered.
 */
bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(initialImage != nullptr && modifiedImage !=nullptr)
    {
        if(isMousedPressedOnInitialImage(obj, event)||isMousedPressedOnModifiedImage(obj,event))
        {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            int mouseX = mouseEvent->pos().x();
            int mouseY = mouseEvent->pos().y();
            inPosX = mouseX;
            inPosY = mouseY;
            mouse_pos pos;
            pos.x = mouseX;
            pos.y = mouseY;
            mouse_clicks.push_back(pos);
            if(mouse_clicks.size() > 4)
                 mouse_clicks.clear();
            qDebug() << QString("Mouse move (%1,%2)").arg(mouseX).arg(mouseY);

            if(isMousedPressedOnInitialImage(obj, event))
            {
                launchMagnifierDialog(mouseX,mouseY,initialImage);
            }else
            {
                launchMagnifierDialog(mouseX,mouseY, modifiedImage);
            }

            if (isMousedPressedOnInitialImage(obj, event)){
                initialClicked = true;
            }
            else{
                initialClicked = false;
            }

            if (graphMode == "grey_level"){
                plotGreyPixels(mouseY, initialImage);
                plotGreyPixels(mouseY,modifiedImage);
            }
            else if (graphMode == "histograma") {
                plotHistogram(mouseY);
            }
            return true;
        }
        else if(isMousedReleasedOnInitialImage(obj, event)||isMousedReleasedOnModifiedImage(obj,event)){
            qDebug() << "mouse released";

            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            int mouseX = mouseEvent->pos().x();
            int mouseY = mouseEvent->pos().y();
            if (mouseX > inPosX){
                outPosX = mouseX;
            }
            else {
                outPosX = inPosX;
                inPosX = mouseX;
            }
            if(mouseY > inPosY){
                outPosY = mouseY;
            }
            else{
                outPosY = inPosY;
                inPosY = mouseY;
            }
            updateImages(false);

            if (graphMode == "varianta"){
                updateVariance();
                Msgbox.exec();
            }
            return true;
        }
    }
    return false;
}

bool MainWindow::isMousedPressedOnInitialImage(QObject *obj, QEvent *event)
{
    return (qobject_cast<QLabel*>(obj) == ui->label) && event->type() == QEvent::MouseButtonPress;
}

bool MainWindow::isMousedReleasedOnInitialImage(QObject *obj, QEvent *event)
{
    return (qobject_cast<QLabel*>(obj) == ui->label) && event->type() == QEvent::MouseButtonRelease;
}

bool MainWindow::isMousedPressedOnModifiedImage(QObject *obj, QEvent *event)
{
    return  qobject_cast<QLabel*>(obj) == ui->label_2 && event->type() == QEvent::MouseButtonPress;
}

bool MainWindow::isMousedReleasedOnModifiedImage(QObject *obj, QEvent *event)
{
    return  qobject_cast<QLabel*>(obj) == ui->label_2 && event->type() == QEvent::MouseButtonRelease;
}


/*
    The method zooms over a 9x9 area that surrounds the given coordinate (x,y).
 */
void MainWindow::launchMagnifierDialog(int x, int y, QImage* image)
{
    if(magnifierDialog == nullptr)
    {
        return;
    }
    magnifierDialog->drawMagnifiedImage(image,x,y);
}

/*
    The method creates a reverted image to the initialImage and puts it in the modifiedImage.
 */
void MainWindow::on_actionRevert_colors_triggered()
{
    if(modifiedImage != nullptr)
    {
        delete modifiedImage;
    }
    modifiedImage = new QImage(initialImage->width(),initialImage->height(),QImage::Format_RGB32);

    if(initialImage == nullptr)
    {
        return;
    }
    for (int i = 0;  i < initialImage->width();  i++)
    {
        for (int j = 0; j < initialImage->height(); j++)
        {
            QColor color = initialImage->pixelColor(i, j);
            modifiedImage->setPixel(i,j,qRgb(255-color.red(),255-color.green(),255-color.blue()));
        }
    }
    modifiedImageLoaded = true;
    updateImages(false);
}

/*
    Sets to visible the greyPlotterDialog.
 */
void MainWindow::on_actionPlot_grey_level_triggered()
{
    graphMode = "grey_level";
    if(greyPlotterDialog==nullptr)
    {
        greyPlotterDialog= new Plotter();
        greyPlotterDialog->activateWindow();
        greyPlotterDialog->setUpPlot();
    }
    greyPlotterDialog->show();
}

/*
   The method plots to the greyPlotDialog the grey level of a given level-y from a given image.
 */
void MainWindow::plotGreyPixels(int y, QImage* image) const
{
    ui->actionHistograma->setChecked(false);

    if(greyPlotterDialog==nullptr)
    {
        return;
    }

    greyPlotterDialog->clearGraph();

    for( int j=0;j<initialImage->width();j++)
    {

        if(image!=nullptr)
        {

            int gray = qGray(image->pixel(j,y));
            if(image==initialImage)
            {
                greyPlotterDialog->plotValuesToFirstGraph(j,gray);
            }else
            {
                greyPlotterDialog->plotValuesToSecondGraph(j,gray);
            }
            qDebug() << QString("qGray (%1)").arg(gray);
        }
    }

    greyPlotterDialog->refresh();
}

/*
   Sets to visible the magnifier dialog.
 */
void MainWindow::on_actionLaunch_magnifier_triggered()
{
    if(magnifierDialog == nullptr)
    {
        magnifierDialog = new Magnifier();
        magnifierDialog->activateWindow();
    }
    magnifierDialog->show();
}

/*
    The gama property launches a dialog that permits saving into the transientData a needed double value.
*/
void MainWindow::on_actionGama_triggered()
{
    bool ok;
    SmartDialog smart("Gama",&ok,4);
    if(ok)
    {
        QMap<QString, double>intermidiateValues= smart.getValues();
        QMap<QString, double>::iterator i;
        QString key;
        for (i = intermidiateValues.begin(); i != intermidiateValues.end(); ++i)
        {
            key=i.key();
            (*transientData)[i.key()]= i.value();
        }
        //Example of usage CTRL+/ to uncomment
        //Make sure you enter a double value!
        //ui->label->setText(QString::number((*transientData)[key]));
    }
}

/*
   oglindeste imaginea
*/
void MainWindow::on_actionOglindire_triggered(){
    modifiedImage = new QImage(initialImage->width(),initialImage->height(),QImage::Format_RGB32);
    for (int i = 0; i < initialImage->width(); i++){
        for (int j = 0; j < initialImage->height();j++){
            QColor color = initialImage->pixelColor(i, j);
            modifiedImage->setPixel(initialImage->width() - 1 - i,j,qRgb(color.red(),color.green(),color.blue()));
        }
    }
    modifiedImageLoaded = true;
    updateImages(false);
}
/*
       binarizare cu doua valori gamma
 */
void MainWindow::on_actionBinarizarea_triggered(){
    if(transientData->size() > 0){
        auto nr = transientData->begin();
        double up = nr.value();
        nr++;
        double low = nr.value();

        if (up < low){
            auto aux = low;
            low = up;
            up = aux;
        }

        modifiedImage = new QImage(initialImage->width(),initialImage->height(),QImage::Format_RGB32);
        for (int i = 0; i< initialImage->width(); i++){
            for (int j = 0; j < initialImage->height();j++){
                QColor color = initialImage->pixelColor(i, j);
                if (color.red()>up){
                    color.setRed(255);
                }
                else if(color.red()<low){
                    color.setRed(0);
                }

                if (color.blue()>up){
                    color.setBlue(255);
                }
                else if(color.blue()<low){
                    color.setBlue(0);
                }

                if (color.green()>up){
                    color.setGreen(255);
                }
                else if(color.green()<low){
                    color.setGreen(0);
                }

                modifiedImage->setPixel(i,j,qRgb(color.red(),color.green(),color.blue()));
            }
        }
        modifiedImageLoaded = true;
        updateImages(false);
    }
    else {
        qDebug()<<" -- no gamma provided";
    }
}
/*

*/
void MainWindow::on_actionSelection_varianta_triggered(){
    graphMode = "varianta";
    Msgbox.setWindowFlags(Qt::Popup);

    updateVariance();
    Msgbox.exec();
}

void MainWindow::updateVariance(){
    QString msg;
    msg = "Initial image: \n";
    msg += calculateVarianceAndAvg(initialImage);
    if (modifiedImageLoaded)
    {
       msg += "\n Modified image: \n";
       msg += calculateVarianceAndAvg(modifiedImage);
    }
    Msgbox.setText(msg);
//    Msgbox.exec();
}

QString MainWindow::calculateVarianceAndAvg(QImage* image){
    double medRed = 0;
    double medBlue = 0;
    double medGreen = 0;

    for (int i = inPosX; i < outPosX; i++){
        for (int j = inPosY; j < outPosY;j++){
            QColor color = image->pixelColor(i, j);
            medRed += color.red();
            medBlue += color.blue();
            medGreen += color.green();
        }
    }

    auto total = (outPosX - inPosX) * (outPosY - inPosY);
    medGreen = medGreen / total;
    medBlue = medBlue / total;
    medRed = medRed / total;

    double sumRed = 0;
    double sumBlue = 0;
    double sumGreen = 0;

    for (int i = inPosX; i< outPosX; i++){
        for (int j = inPosY; j < outPosY;j++){
            QColor color = image->pixelColor(i, j);
            sumRed += pow((color.red() - medRed), 2) / total;
            sumBlue += pow((color.blue() - medBlue), 2) / total;
            sumGreen += pow((color.green() - medGreen), 2) / total;
        }
    }

    QString msg = QString("");
    msg = msg + QString("Variance R: %1 \n").arg(sqrt(sumRed));
    msg = msg + QString("Variance G: %1 \n").arg(sqrt(sumGreen));
    msg = msg + QString("Variance B: %1 \n \n").arg(sqrt(sumBlue));
    msg = msg + QString("Media R: %1 \n").arg(medRed);
    msg = msg + QString("Media G: %1 \n").arg(medGreen);
    msg = msg + QString("Media B: %1 \n").arg(medBlue);
    return msg;
//    Msgbox.setText(msg);
}

void MainWindow::on_actionHistograma_triggered(){
    graphMode = "histograma";

    greyPlotterDialog = new Plotter();
    greyPlotterDialog->activateWindow();
    greyPlotterDialog->setUpPlot();
    greyPlotterDialog->show();

    plotHistogram();
}

void MainWindow::plotHistogram(int line){
    ui->actionPlot_grey_level->setChecked(false);

    int valRed[255] = {0};
    int valBlue[255] = {0};
    int valGreen[255] = {0};
    int iStart = 0;
    QImage* image;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    int width = image->width();
    int height = image->height();

    if (line != -1){
        iStart = line;
        height = line +1;
    }

    if (!isGreyscale){
    for (int i = iStart; i < height; i++){
        for (int j = 0; j < width;j++){
            QColor color = image->pixelColor(j, i);
            valRed[color.red() - 1] += 1;
            valGreen[color.green() - 1] += 1;
            valBlue[color.blue() - 1] += 1;
        }
    }
    }
    else {
        for (int i = iStart; i < height; i++){
            for (int j = 0; j < width;j++){
                QColor color = initialImage->pixelColor(j, i);
                valBlue[color.red() - 1] += 1;
                color = modifiedImage->pixelColor(j, i);
                valRed[color.blue() - 1] += 1;
            }
        }
    }

    //std::sort(valRed, valRed + 255);
    //std::sort(valBlue, valBlue + 255);
    //std::sort(valGreen, valGreen + 255);

    greyPlotterDialog->clearGraph();

    for( int j=0;j<255;j++)
    {
       greyPlotterDialog->plotValuesToFirstGraph(j,valBlue[j]);
       greyPlotterDialog->plotValuesToSecondGraph(j,valRed[j]);
       if (!isGreyscale)
           greyPlotterDialog->plotValuesToThirdGraph(j,valGreen[j]);
    }

    greyPlotterDialog->refresh();
}

// radians = ( degrees * pi ) / 180 ;

// Converting from radians to degrees

// degrees=( radians * 180 ) / pi ;

//  Red = H
//  Blue = S
//  Green = V

void MainWindow::on_actionColor_Histogram_Equalization_triggered(){
    QImage* image;
    modifiedImageLoaded = true;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    int width = image->width();
    int height = image->height();
    std::vector<std::vector<double>> vecH(width);
    std::vector<std::vector<double>> vecS(width);
    std::vector<std::vector<double>> vecV(width);

    auto vec = RGBtoHSV(image, width, height);
    vecH = vec[0];
    vecS = vec[1];
    vecV = vec[2];

    double valV[255] = {0};
    double total = width * height;
    for (int i = 0; i < width; i++){
        for (int j = 0; j < height;j++){
            // histogram equalization for grey pics only
            QColor color = image->pixelColor(i, j);
            valV[color.red() - 1] += 1;
        }
    }

    for (int i = 0; i < 255; i++ ){
        valV[i] /= total;
    }

    double tableV[255] = {0};
    double min = *std::min_element(valV, valV + 255);
    for (int i = 0; i< 255; i++){
        double summ = 0;
        for (int j = 0 ; j<=i; j++){
            summ += valV[j] / (1 - min);
        }
        tableV[i] = summ - (min / (1 - min));
    }
    for (int i = 0; i < width; i++){
        for (int j = 0; j < height;j++){
            int pos = round(vecV[i][j] * 255);
            if (pos >= 255){
                pos-=1;
            }
            vecV[i][j] = tableV[pos];
        }
    }

    modifiedImage = HSVtoRGB(width, height, vecH, vecS, vecV);
    updateImages(false);
}

std::vector<std::vector<std::vector<double>>> MainWindow::RGBtoHSV(QImage* image, int width, int height){
    double vec[3] = {0};
    double h,s,v;

    double min = 255;
    double max = 0;

    std::vector<std::vector<double>> vec2H(width);
    std::vector<std::vector<double>> vec2S(width);
    std::vector<std::vector<double>> vec2V(width);
    std::vector<std::vector<std::vector<double>>> vec2(0);

    // to hsv
    for (int i = 0; i < width; i++){
        for (int j = 0; j < height;j++){
            QColor color = image->pixelColor(i, j);

            vec[0] = color.red() / 255.0;
            vec[1] = color.blue() / 255.0;
            vec[2] = color.green() / 255.0;

            max = *std::max_element(vec, vec + 3);
            min = *std::min_element(vec, vec + 3);

            if (vec[0] == vec[1] && vec[1] == vec[2]){
                h = 0.0;
            }
            else if (max <= vec[0]){
                h = 60 * (0 + ((vec[2] - vec[1]) / (max - min)));
            }
            else if (max <= vec[2]){
                h = 60 * (2 + (vec[1] - vec[0]) / (max - min));
            }
            else if (max <= vec[1]){
                h = 60 * (4 + (vec[0] - vec[2]) / (max - min));
            }

            if (h <= 0.0){
                h += 360;
            }

            if (max <= 0.0){
                s = 0.0;
            }
            else {
                s = (max - min) / max;
            }
            v = max;
            //qDebug() << (double)h << " " << (double)s << " " << (double)v;
            vec2H[i].push_back(h);
            vec2S[i].push_back(s);
            vec2V[i].push_back(v);
        }
    }
    vec2.push_back(vec2H);
    vec2.push_back(vec2S);
    vec2.push_back(vec2V);

    return vec2;
}

QImage * MainWindow::HSVtoRGB(int width, int height, std::vector<std::vector<double>> vecH, std::vector<std::vector<double>> vecS, std::vector<std::vector<double>> vecV){
    long double C=0.0; // v * s
    long double invH = 0.0; // h / 60
    long double X = 0.0; // c * (1 - |h' mod 2 - 1 |)
    long double m = 0.0; // v - c
    long double r,g,b;

    QImage *hsv = new QImage(width,height,QImage::Format_RGB32);

    for (int i = 0; i < width; i++){
        for (int j = 0; j < height;j++){
            C = vecV[i][j] * vecS[i][j];
            invH = vecH[i][j] / 60.0;
            X = C * (1 - fabs(fmod(invH, 2) - 1));

            if (vecH[i][j] == INFINITY){
                r = 0.0;
                g = 0.0;
                b = 0.0;
            }
            if(invH <= 1 && invH >= 0){
                r = C;
                g = X;
                b = 0.0;
            }
            else if(invH <= 2 && invH >= 1){
                r = X;
                g = C;
                b = 0.0;
            }
            else if(invH <= 3 && invH >= 2){
                r = 0.0;
                g = C;
                b = X;
            }
            else if(invH <= 4 && invH >= 3){
                r = 0.0;
                g = X;
                b = C;
            }
            else if(invH <= 5 && invH >= 4){
                r = X;
                g = 0.0;
                b = C;
            }
            else if(invH <= 6 && invH >= 5){
                r = C;
                g = 0.0;
                b = X;
            }
            else {
                r = 0.0;
                g = 0.0;
                b = 0.0;
            }

            m = vecV[i][j] - C;

            r += m;
            g += m;
            b += m;

            r *= 255;
            g *= 255;
            b *= 255;

            hsv->setPixel(i,j, qRgb(round(r), round(g), round(b)));
        }
    }

    return hsv;
}
void MainWindow::on_actionBilateral_filtering_triggered(){

    qDebug() << "bilateral";
    QImage* image;
    modifiedImageLoaded = true;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    int width = image->width();
    int height = image->height();
    int box = 10;  // box /2 => 3
    if (transientData->size() > 0){


    auto nr = transientData->begin();
    double sigmaD = nr.value();
    nr++;
    double sigmaR = nr.value();

    // create hd matrix
    std::vector<std::vector<double>> hdV(box * 2 + 2);
    double pi = 3.14159265359;

//    for (int i = 0; i <= box * 2; i++){
//        for (int j = 0; j <= box * 2; j++){
//            hdV[i].push_back((1/(2*pi*sigmaD*sigmaD)) *exp(-(((i)*(i) + (j)*(j))/(2 * sigmaD * sigmaD))));

//        }
//    }
    double k = box - 1;
    for (int i=0;i<=box*2 + 1; i++){
        for (int j=0; j<=box*2 + 1; j++){
            double part1 = (i - (k + 1)) * (i - (k + 1));
            double part2 = (j - (k + 1)) * (j - (k + 1));
            double nr = ((1/(2*pi*sigmaD*sigmaD)) * exp(-(part1 + part2)/ (2*sigmaD*sigmaD)));
//            if (nr> 1){
//                nr = 1;
//            }
            hdV[i].push_back(nr);

        }
    }

    for (int i = 0; i < width; i++){
        for (int j = 0; j < height;j++){
            ui->progressBar->setValue(i*100/(width-1));
            QCoreApplication::processEvents();
            QColor Ix = image->pixelColor(i, j);
            double rUp = 0;
            double gUp = 0;
            double bUp = 0;
            double rDown = 0;
            double gDown = 0;
            double bDown = 0;
            double hrR = 0;
            double hrG = 0;
            double hrB = 0;

            for (int k = 0; k <= box * 2; k++){
                for (int l = 0; l<= box * 2; l++){
                    if (i+k-box >= 0 && j+l - box >= 0 && i+k-box < width && j + l - box < height){
                    QColor Ixi = image->pixelColor(i+k-box, j+l-box);


                    // this should be calculated separated (hd)
//                    double hd = exp(-(((i-k)*(i-k) + (j-l)*(j-l))/(2 * sigmaD * sigmaD)));
                    //qDebug() << i-k+box << " " << j - l+box;
                    double hd = hdV[k+1][l+1];
                    double deltaR;
                    double deltaG;
                    double deltaB;
                    deltaR = abs(Ixi.red() - Ix.red());
                    deltaB = abs(Ixi.blue() - Ix.blue());
                    deltaG = abs(Ixi.green() - Ix.green());

                    hrR += exp(-((deltaR * deltaR) / (2 * sigmaR * sigmaR)));
                    hrG += exp(-((deltaG * deltaG) / (2 * sigmaR * sigmaR)));
                    hrB += exp(-((deltaB * deltaB) / (2 * sigmaR * sigmaR)));

                    rUp += (Ixi.red() * hd * hrR);
                    gUp += (Ixi.green() * hd * hrG);
                    bUp += (Ixi.blue() * hd * hrB);

                    rDown += (hd*hrR);
                    gDown += (hd*hrG);
                    bDown += (hd*hrB);
                    }
                }

            }

                modifiedImage->setPixel(i, j, qRgb(round(rUp/rDown), round(gUp/gDown), round(bUp/bDown)));
        }
    }
    updateImages(false);
    }
    else{
        qDebug() << "no gammas provided";
    }
}

void MainWindow::on_actionCany_edge_detection_triggered(){
    qDebug() << "canny";
    QImage* image;
    modifiedImageLoaded = true;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    if (transientData->size() > 0){
        auto nr = transientData->begin();
        double sigmaD = nr.value();
        nr++;
        double sigmaR = nr.value();
        nr++;
        double q = nr.value();

        modifiedImage = gaussian_fiter(q, image);
        updateImages(false);
        modifiedImage = canny_edge_detection(sigmaD, sigmaR, modifiedImage);
        updateImages(false);
    }
    else {
        qDebug() << "no T1 and T2 or Q_3";
    }
}

void MainWindow::on_actionSwap_Images_triggered(){
    if (modifiedImageLoaded)
        initialImage = new QImage(*modifiedImage);
    updateImages(false);
}

QImage * MainWindow::canny_edge_detection(double T1, double T2, QImage * img){
    QImage* cannyImage = new QImage(img->width(),img->height(),QImage::Format_RGB32);
    std::vector<std::vector<double>> values;
    std::vector<std::vector<double>> orientation;

///////////// for tests
///
//    auto norma_sobel = [&](double val1, double val2){
//        return sqrt(pow(val1,2) + pow(val2,2));
//    };
//    auto norma = [](QColor color){
//        return sqrt(pow(color.redF(), 2) + pow(color.greenF(), 2) + pow(color.blueF(), 2));
//    };

//    auto scalar_product = [](QColor c1, QColor c2){
//        return c1.redF()*c2.redF() + c1.greenF()*c2.greenF() + c1.blueF()*c2.blueF();
//    };

//    auto euclidean_dist = [&](QColor color1, QColor color2){
//        QColor diff;
//        diff.setRed(abs(color1.red() - color2.red()));
//        diff.setGreen(abs(color1.green() - color2.green()));
//        diff.setBlue(abs(color1.blue() - color2.blue()));

//        return norma(diff);
//    };

//    auto vectorial_angle = [&](QColor color1, QColor color2){
//        return sqrt(1 - pow(scalar_product(color1, color2) / (norma(color1) * norma(color2)) ,2));
//    };

//    auto difference_vector_edge_detection = [&](int i, int j){
//        double currentEDV = 0;

//        // values
//        double compareEDV = euclidean_dist(img->pixelColor(i+1 , j+1), img->pixelColor(i-1 , j-1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = euclidean_dist(img->pixelColor(i , j+1), img->pixelColor(i , j-1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = euclidean_dist(img->pixelColor(i+1 , j-1), img->pixelColor(i-1 , j+1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = euclidean_dist(img->pixelColor(i+1 , j), img->pixelColor(i-1 , j));
//        currentEDV = std::max(currentEDV, compareEDV);

//        // angles
//        compareEDV = vectorial_angle(img->pixelColor(i+1 , j+1), img->pixelColor(i-1 , j-1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = vectorial_angle(img->pixelColor(i , j+1), img->pixelColor(i , j-1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = vectorial_angle(img->pixelColor(i+1 , j-1), img->pixelColor(i-1 , j+1));
//        currentEDV = std::max(currentEDV, compareEDV);
//        compareEDV = vectorial_angle(img->pixelColor(i+1 , j), img->pixelColor(i-1 , j));
//        currentEDV = std::max(currentEDV, compareEDV);

//        return currentEDV; // + angle
//    };
///////// exit test

    auto sobel_x_red = [&](int i, int j){
        return
        img->pixelColor(i+1 , j-1).redF() -
        img->pixelColor(i-1 , j-1).redF() +
        2 * img->pixelColor(i+1 , j).redF() -
        2 * img->pixelColor(i-1 , j).redF() +
        img->pixelColor(i+1 , j+1).redF() -
        img->pixelColor(i-1 , j+1).redF();
    };

    auto sobel_y_red = [&](int i, int j){
        return
        img->pixelColor(i-1 , j+1).redF() -
        img->pixelColor(i-1 , j-1).redF() +
        2 * img->pixelColor(i, j+1).redF() -
        2 * img->pixelColor(i, j-1).redF() +
        img->pixelColor(i+1 , j+1).redF() -
        img->pixelColor(i+1 , j-1).redF();
    };

    auto sobel_x_green = [&](int i, int j){
        return
        img->pixelColor(i+1 , j-1).greenF() -
        img->pixelColor(i-1 , j-1).greenF() +
        2 * img->pixelColor(i+1 , j).greenF() -
        2 * img->pixelColor(i-1 , j).greenF() +
        img->pixelColor(i+1 , j+1).greenF() -
        img->pixelColor(i-1 , j+1).greenF();
    };

    auto sobel_y_green = [&](int i, int j){
        return
        img->pixelColor(i-1 , j+1).greenF() -
        img->pixelColor(i-1 , j-1).greenF() +
        2 * img->pixelColor(i, j+1).greenF() -
        2 * img->pixelColor(i, j-1).greenF() +
        img->pixelColor(i+1 , j+1).greenF() -
        img->pixelColor(i+1 , j-1).greenF();
    };

    auto sobel_x_blue = [&](int i, int j){
        return
        img->pixelColor(i+1 , j-1).blueF() -
        img->pixelColor(i-1 , j-1).blueF() +
        2 * img->pixelColor(i+1 , j).blueF() -
        2 * img->pixelColor(i-1 , j).blueF() +
        img->pixelColor(i+1 , j+1).blueF() -
        img->pixelColor(i-1 , j+1).blueF();
    };

    auto sobel_y_blue = [&](int i, int j){
        return
        img->pixelColor(i-1 , j+1).blueF() -
        img->pixelColor(i-1 , j-1).blueF() +
        2 * img->pixelColor(i, j+1).blueF() -
        2 * img->pixelColor(i, j-1).blueF() +
        img->pixelColor(i+1 , j+1).blueF() -
        img->pixelColor(i+1 , j-1).blueF();
    };

    auto verify_neighbors = [&values](int i, int j){
        if (values[i+1][j-1] == 255 ||
                values[i+1][j+1] == 255 ||
                values[i-1][j+1] == 255 ||
                values[i-1][j-1] == 255 ||
                values[i-1][j] == 255 ||
                values[i+1][j] == 255 ||
                values[i][j+1] == 255 ||
                values[i][j-1] == 255)
            return 255;
        else
            return 0;
    };

    // hystesis
    auto canny_compare = [&](double val, int i, int j){
        if(val <= T1)
            return 0;
        else if (val >= T2)
            return 255;
        else if (val > T1 && val < T2)
            return verify_neighbors(i, j);
        else
            return 127;
    };

    auto non_maxima_suppresion = [&](double orientation, int i, int j){
        if((orientation < M_PI/2. && orientation > 3.*M_PI/8.) || (orientation > -M_PI/2. && orientation < -3.*M_PI/8.)){
            if(values[i][j] > values[i][j + 1]){
                values[i][j + 1] = 0;
            }
            if(values[i][j] > values[i][j - 1]){
                values[i][j - 1] = 0;
            }
        }
        else if (orientation < M_PI/8. && orientation > -M_PI/8.){
            if(values[i][j] > values[i - 1][j]){
                values[i - 1][j] = 0;
            }
            if(values[i][j] > values[i + 1][j]){
                values[i + 1][j] = 0;
            }
        }
        else if (orientation < 3*M_PI/8 && orientation > M_PI/8){
            if(values[i][j] > values[i - 1][j - 1]){
                values[i - 1][j - 1] = 0;
            }
            if(values[i][j] > values[i + 1][j + 1]){
                values[i + 1][j + 1] = 0;
            }
        }
        else if (orientation > -3*M_PI/8 && orientation < -M_PI/8){
            if(values[i][j] > values[i + 1][j - 1]){
                values[i + 1][j - 1] = 0;
            }
            if(values[i][j] > values[i - 1][j + 1]){
                values[i - 1][j + 1] = 0;
            }
        }
    };

    for (int i = 0; i < img->width(); i++){
        values.push_back(std::vector<double>());
        orientation.push_back(std::vector<double>());
         for (int j = 0; j < img->height();j++){
             ui->progressBar->setValue(50 + i*100/(img->width()-1) / 4);
             if(i > 0 && i < img->width() - 1 && j > 0 && j < img->height() -1){
                 //double current = difference_vector_edge_detection(i,j);

                 double gxx = pow(sobel_x_red(i,j), 2) + pow(sobel_x_green(i,j), 2) + pow(sobel_x_blue(i,j), 2);
                 double gyy = pow(sobel_y_red(i,j), 2) + pow(sobel_y_green(i,j), 2) + pow(sobel_y_blue(i,j), 2);
                 double gxy = sobel_x_red(i,j) * sobel_y_red(i,j) + sobel_x_green(i,j) * sobel_y_green(i,j) + sobel_x_blue(i,j) * sobel_y_blue(i,j);

                 double current = sqrt((gxx + gyy + sqrt(pow(gxx - gyy,2) + 4*pow(gxy,2)))/2);
                 double idk = (gxx - gyy + sqrt(pow(gxx - gyy,2) + 4*pow(gxy,2)));
                 double dirrection = atan((2*gxy) / idk);//atan((2 * gxy)/(gxx - gyy))/2;

                 values[i].push_back(current);
                 orientation[i].push_back(dirrection);
             }
             else {
                 values[i].push_back(0);
                 orientation[i].push_back(0);
             }
         }
    }

    for (int i = 0; i < img->width(); i++){
         for (int j = 0; j < img->height();j++){
             if(i > 0 && i < img->width() - 1 && j > 0 && j < img->height() -1){
                non_maxima_suppresion(orientation[i][j], i,j);
             }
         }
    }

    for (int i = 0; i < img->width(); i++){
        ui->progressBar->setValue(75 + i*100/(img->width()-1)/ 4);
         for (int j = 0; j < img->height();j++){
             QColor newPixel;
             newPixel.setRed(0);
             newPixel.setGreen(0);
             newPixel.setBlue(0);

             newPixel.setRed(canny_compare(values[i][j], i, j));
             newPixel.setGreen(canny_compare(values[i][j], i, j));
             newPixel.setBlue(canny_compare(values[i][j], i, j));

             cannyImage->setPixel(i,j,qRgb(newPixel.red(), newPixel.green(), newPixel.blue()));
         }
    }

    return cannyImage;
}

QImage * MainWindow::gaussian_fiter(double q, QImage * img){
    double pi = 3.14159265359;
    double box = 8;
    std::vector<std::vector<double>> gaus(box + 1);
    double k = box/2;
    QImage* gausianImage = new QImage(img->width(),img->height(),QImage::Format_RGB32);

    for (int i=0;i<box; i++){
        for (int j=0; j<box; j++){
            double part1 = (i - (k + 1)) * (i - (k + 1));
            double part2 = (j - (k + 1)) * (j - (k + 1));
            double nr = ((1/(2*pi*q*q)) * exp(-(part1 + part2)/ (2*q*q)));

            gaus[i].push_back(nr);

        }
    }

    for (int i = 0; i < img->width(); i++){
         for (int j = 0; j < img->height();j++){
             double r = 0;
             double g = 0;
             double b = 0;
             double nr = 0;
             ui->progressBar->setValue(i*100/(img->width()-1)/2);
             for (int k = 0; k< box; k++){
                 for (int l = 0; l < box; l++){
                     if ( i-k+ box/2<img->width() && j-l+ box/2<img->height() && i-k+ box/2 >= 0 && j-l+ box/2>= 0){
                         QColor otherPixel = img->pixelColor(i-k+ box/2 , j-l+ box/2);
                         r += (otherPixel.red() * gaus[k][l]);
                         g += (otherPixel.green() * gaus[k][l]);
                         b += (otherPixel.blue() * gaus[k][l]);
                         nr += gaus[k][l];
                     }
                 }
             }
             gausianImage->setPixel(i, j, qRgb(round(r / (nr)) , round(g / (nr)), round(b / (nr))));
        }
    }

    return gausianImage;
}

void MainWindow::on_actionWatershed_triggered(){
    qDebug() << "watershed";
    QImage* image;
    modifiedImageLoaded = true;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    if (transientData->size() > 0){
        auto nr = transientData->begin();
        double T = nr.value();

        modifiedImage = watershed(image, T);
        updateImages(false);
    }
    else {
        qDebug() << "no T";
    }
}

///         steps
///  1. trashold colors refering to T
///  2. process data in priority! queue that are good for current level
///  3. select one minima that can be inserted in que for current level
///  4. if que haves only next level data increment current level goto 2. if queue not empty
///

QImage * MainWindow::watershed(QImage * img, int T){
    struct pix_pos{
        int x;
        int y;
        bool operator==(const pix_pos& lhs)
        {
            if(lhs.x == this->x && lhs.y == this->y)
                return 1;
            else
                return 0;
        }
        operator QString() const { return QString::number(x) + " " + QString::number(y); }
    };

    struct water {
        int level;
        int value;
        int label;
    };

    std::vector<std::vector<water>> pixel_classification;

    QImage* watershedImage = new QImage(img->width(),img->height(),QImage::Format_RGB32);
    std::vector<int> T_bound;
    std::vector<pix_pos> priority_stack;
    int number_of_labels = 0;

    for (int i=0; i<T; i++){
        T_bound.push_back(round(255./T + (255./T)*i));
    }

    // sets level in function of pixel color
    auto set_label = [&](int i, int j){
        QColor pixel = img->pixelColor(i, j);
        for (int k=0; k < T; k++){
            if (pixel.red() < T_bound[k] + 1){
                pixel_classification[i][j].level = T_bound[k];
                pixel_classification[i][j].label = 0;
                pixel_classification[i][j].value = pixel.red();
                QColor pixel = img->pixelColor(i,j);
                watershedImage->setPixel(i,j, qRgb(pixel.red(), pixel.green(), pixel.blue()));

                break;
            }
        }
    };

    // does have different labeled neighbors? set label to 255 : insert in vector
    auto verify_neighbors = [&](int current_i, int current_j, int neighbor_i, int neighbor_j){
        if(neighbor_i < img->width() && neighbor_i >= 0 && neighbor_j < img->height() && neighbor_j >= 0){
        water neightbor_pixel = pixel_classification[neighbor_i][neighbor_j];
                if(neightbor_pixel.label == 0)
                {
                    pixel_classification[neighbor_i][neighbor_j].label = pixel_classification[current_i][current_j].label;
                    pix_pos pos;
                    pos.x= neighbor_i;
                    pos.y = neighbor_j;

                    auto itr = std::find(priority_stack.begin(), priority_stack.end(), pos);
                    if (itr == priority_stack.end())
                        priority_stack.push_back(pos);
                }
        }

        //auto last = std::unique(priority_stack.begin(), priority_stack.end());
        //priority_stack.erase(last, priority_stack.end());
    };

    // search minima on current level if there is unlabeled data on current level
    auto search_minima = [&](int current_level){
        int min = 255;
        pix_pos min_pos;
        min_pos.x = -1;
        for (int i = 0; i < img->width(); i++){
            for (int j = 0; j < img->height();j++){
                    water pixel = pixel_classification[i][j];
                    if (pixel.value < min &&
                            pixel.label == 0 &&
                            current_level == pixel.level){
                        min = pixel.value;
                        min_pos.x = i;
                        min_pos.y = j;
                    }
            }
        }

        // insert it in que
        if(min_pos.x != -1){
            pixel_classification[min_pos.x][min_pos.y].value = 256;
            number_of_labels++;
            pixel_classification[min_pos.x][min_pos.y].label = number_of_labels;
            priority_stack.push_back(min_pos);
        }
        return min_pos;
    };

    // searches for smallest color value available in the priority vector
    auto get_next_from_stack = [&pixel_classification, &priority_stack](int current_level){
        int min = 257;
        pix_pos pos_min;
        pos_min.x = -1;
        pos_min.y = -1;
        for (pix_pos pos : priority_stack){
            water pixel = pixel_classification[pos.x][pos.y];
            if (pixel.value < min && pixel.level == current_level){
                min = pixel.value;
                pos_min = pos;
            }
        }

        if(pos_min.x != -1){
            auto itr = std::find(priority_stack.begin(), priority_stack.end(), pos_min);
            if (itr != priority_stack.end())
                priority_stack.erase(itr);
        }
        return pos_min;
    };

    // legend -> green chanel is used as level
    // legend -> blue label
    // legend -> red for minima = 255 / color

    // sets tresholding
    for (int i = 0; i < img->width(); i++){
        pixel_classification.push_back(std::vector<water>());
        for (int j = 0; j < img->height();j++){
            pixel_classification[i].push_back(water());
            set_label(i,j);
        }
    }

    // main loop
    int level = 0;
    int i=0;
    while (search_minima(T_bound[level]).x == -1){level++;}
    while(priority_stack.empty() != true){
        ui->progressBar->setValue(i*100/(img->width() * img->height()));
        if(level == T){
            qDebug() << "segmentation error";
            break;
        }
        auto current = get_next_from_stack(T_bound[level]);
        if(current.x == -1){
            if(search_minima(T_bound[level]).x == -1){
                level++;
            }
        }
        else{
            verify_neighbors(current.x, current.y, current.x + 1, current.y);
            verify_neighbors(current.x, current.y, current.x - 1, current.y);
            verify_neighbors(current.x, current.y, current.x, current.y + 1);
            verify_neighbors(current.x, current.y, current.x, current.y - 1);
            verify_neighbors(current.x, current.y, current.x + 1, current.y + 1);
            verify_neighbors(current.x, current.y, current.x + 1, current.y - 1);
            verify_neighbors(current.x, current.y, current.x - 1, current.y - 1);
            verify_neighbors(current.x, current.y, current.x - 1, current.y + 1);
        }
        i++;
    }

    // results interpretation
    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            bool do_blue = false;
            bool set = false;
            pix_pos pos;
            if(pixel_classification[i][j].value == 256){
                watershedImage->setPixel(i,j, qRgb(255, 0, 0));
                set = true;
            }
            if(i+1 < img->width()){
                if(pixel_classification[i][j].label != pixel_classification[i + 1][j].label){
                    do_blue = true;
                    pos.x = i;
                    pos.y = j;
                }
            }
            if(j+1 < img->height()){
                if(pixel_classification[i][j].label != pixel_classification[i][j + 1].label){
                    do_blue = true;
                    pos.x = i;
                    pos.y = j;
                }
            }
            if(i+1 < img->width() && j+1 < img->height()){
                if(pixel_classification[i][j].label != pixel_classification[i + 1][j + 1].label){
                    do_blue = true;
                    pos.x = i;
                    pos.y = j;
                }
            }

            if(do_blue && !set){
                watershedImage->setPixel(pos.x,pos.y, qRgb(0, 0, 255));
            }
        }
    }

    return watershedImage;
}


// detect connex components on binary images
void MainWindow::on_actionConex_component_triggered(){
    qDebug() << "conex";
    QImage* image;
    modifiedImageLoaded = true;

    if (initialClicked){
        image = initialImage;
    }
    else {
        image = modifiedImage;
    }

    modifiedImage = conex_component(image);
    updateImages(false);
}

QImage* MainWindow::conex_component(QImage * img){ 
    QImage* conexImage = new QImage(img->width(),img->height(),QImage::Format_RGB32);
    std::vector<std::vector<int>> connections;
    std::vector<std::vector<int>> img_map;
    int components_number = 0;
    int connections_number = 0;

    auto verify_neighbor = [&](int neighbor_i, int neighbor_j, int current_i, int current_j){
          if(neighbor_i < img->width() && neighbor_i >= 0 && neighbor_j < img->height() && neighbor_j >= 0){
              if (img_map[neighbor_i][neighbor_j] > 0){
                  if(img_map[current_i][current_j] == -1){
                      img_map[current_i][current_j] = img_map[neighbor_i][neighbor_j];
                  }
              }
              else if(img_map[neighbor_i][neighbor_j] == -1 && img_map[current_i][current_j] > 0){
                  img_map[neighbor_i][neighbor_j] = img_map[current_i][current_j];
              }
          }
    };

    auto find_connected_index = [&](int conn){
        if(conn == 0)
            return -2;
        for(int i = 0; i < (int)connections.size(); i++){
            auto it = std::find (connections[i].begin(), connections[i].end(), conn);
            if (it != connections[i].end()){
                return i;
            }
        }
        return -1;
    };

    auto verify_connection = [&](int neighbor_i, int neighbor_j, int current_i, int current_j){
        if(neighbor_i < img->width() && neighbor_i >= 0 && neighbor_j < img->height() && neighbor_j >= 0){
            if(img_map[neighbor_i][neighbor_j] > 0 && img_map[current_i][current_j] > 0){
                int found_on_neigh = find_connected_index(img_map[neighbor_i][neighbor_j]);
                int found_on_current = find_connected_index(img_map[current_i][current_j]);
                bool set = false;
                if (found_on_neigh == -1 && found_on_current == -1){
                    connections.push_back(std::vector<int>());
                    connections[connections_number].push_back(img_map[neighbor_i][neighbor_j]);
                    if(img_map[neighbor_i][neighbor_j] != img_map[current_i][current_j]){
                        connections[connections_number].push_back(img_map[current_i][current_j]);
                    }
                    connections_number++;
                    set = true;
                }
                if(found_on_neigh >= 0 && found_on_current >= 0 && found_on_neigh != found_on_current && !set){
                    connections[found_on_neigh].push_back(img_map[current_i][current_j]);
                    connections[found_on_current].push_back(img_map[neighbor_i][neighbor_j]);
                    set = true;
                }
                if(found_on_current >= 0 && !set){
                    if(found_on_current != found_on_neigh){
                        connections[found_on_current].push_back(img_map[neighbor_i][neighbor_j]);
                        set = true;
                    }
                }
                if (found_on_neigh >= 0 && !set){
                    if(found_on_current != found_on_neigh){
                        connections[found_on_neigh].push_back(img_map[current_i][current_j]);
                        set = true;
                    }
                }
            }
        }
    };

    // setup image map
    for (int i = 0; i < img->width(); i++){
        img_map.push_back(std::vector<int>());
        for (int j = 0; j < img->height();j++){
            QColor pixel = img->pixelColor(i, j);
            if(pixel.red() > 200){
                img_map[i].push_back(-1);
            }
            else {
                img_map[i].push_back(0);
            }
        }
    }

    // calculate initial components
    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            if(img_map[i][j] == -1){
                verify_neighbor(i+1, j, i, j);
                verify_neighbor(i-1, j, i, j);
                verify_neighbor(i, j+1, i, j);
                verify_neighbor(i, j-1, i, j);
                verify_neighbor(i-1, j-1, i, j);
                verify_neighbor(i-1, j+1, i, j);
                verify_neighbor(i+1, j-1, i, j);
                verify_neighbor(i+1, j+1, i, j);
            }

            if(img_map[i][j] == -1) {
                components_number++;
                img_map[i][j] = components_number;
            }
        }
    }

    // connect components
    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            if(img_map[i][j] != 0){
                verify_connection(i+1, j, i, j);
                verify_connection(i-1, j, i, j);
                verify_connection(i, j+1, i, j);
                verify_connection(i, j-1, i, j);
                verify_connection(i-1, j-1, i, j);
                verify_connection(i-1, j+1, i, j);
                verify_connection(i+1, j-1, i, j);
                verify_connection(i+1, j+1, i, j);
            }
        }
    }
    int clusters = connections.size();
    qDebug() << "There are connected clusters: " << clusters;

    int colors[3] = {250,250,250};

    // merge clusters
    for (int l=0 ; l < (int)connections.size(); l++){
    for (int i = l+1 ; i< (int)connections.size(); i++){
        bool set = false;
        for (int j=0; j< (int)connections[i].size(); j++){
            for (int k=0; k< (int)connections[l].size(); k++){
                if (connections[i][j] == connections[l][k]){
                    connections[l].insert(connections[l].begin(), connections[i].begin(), connections[i].end());
                    connections[i].clear();
                    clusters--;
                    set = true;
                    break;
                }
            }
            if (set)
                break;
        }
    }
    }

    qDebug() << "There connex elements: " << clusters;

    // find color on image
    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            QColor pixel;
            pixel.setRed(0);
            pixel.setGreen(0);
            pixel.setBlue(0);

            int index = find_connected_index(img_map[i][j]);
            if(index!=-1){
                int min = 0;
                if (index < 200){
                    min = index;
                }

            switch (index%6) {
            case 0:
                pixel.setRed(colors[0] - min);
                break;
            case 1:
                pixel.setGreen(colors[1] - min);
                break;
            case 2:
                pixel.setBlue(colors[2] - min);
                break;
            case 3:
                pixel.setGreen(colors[1] - min);
                pixel.setBlue(colors[2] - min);
                break;
            case 4:
                pixel.setRed(colors[0] - min);
                pixel.setBlue(colors[2] - min);
                break;
            case 5:
                pixel.setRed(colors[0] - min);
                pixel.setGreen(colors[1] - min);
                break;
            }
            }
            conexImage->setPixel(i,j, qRgb(pixel.red(), pixel.green(), pixel.blue()));
        }
    }

    return conexImage;
}

void MainWindow::on_actionTransformare_proiectiva_triggered(){
    qDebug() << "transform";
    showGaphTransform = true;
    if(mouse_clicks.size() >= 4){
        QImage* image;
        modifiedImageLoaded = true;

        if (initialClicked){
            image = initialImage;
        }
        else {
            image = modifiedImage;
        }

        modifiedImage = transformare_proiectiva(image);
        updateImages(false);
    }
}

// define data tyeps boost
#include <boost/qvm/mat_traits.hpp>
#include <boost/qvm/vec_operations.hpp>
#include <boost/qvm/vec_traits_array.hpp>
#include <boost/qvm/mat_traits_array.hpp>
#include <boost/qvm/vec.hpp>
#include <boost/qvm/enable_if.hpp>

struct float33 { float a[3][3]; };
namespace boost { namespace qvm {

  template <>
  struct mat_traits<float33> {

    static int const rows=3;
    static int const cols=3;
    typedef float scalar_type;

    template <int R,int C>
    static inline scalar_type & write_element( float33 & m ) {
      return m.a[R][C];
    }

    template <int R,int C>
    static inline scalar_type read_element( float33 const & m ) {
      return m.a[R][C];
    }

    static inline scalar_type & write_element_idx( int r, int c, float33 & m ) {
      return m.a[r][c];
    }

    static inline scalar_type read_element_idx( int r, int c, float33 const & m ) {
      return m.a[r][c];
    }

  };

} }

struct float3 { float a[3]; };

namespace boost { namespace qvm {

  template <>
  struct vec_traits<float3> {

    static int const dim=3;
    typedef float scalar_type;

    template <int I>
    static inline scalar_type & write_element( float3 & v ) {
      return v.a[I];
    }

    template <int I>
    static inline scalar_type read_element( float3 const & v ) {
      return v.a[I];
    }

    static inline scalar_type & write_element_idx( int i, float3 & v ) {
      return v.a[i];
    } //optional

    static inline scalar_type read_element_idx( int i, float3 const & v ) {
      return v.a[i];
    } //optional

  };

} }

namespace boost { namespace qvm {
template <class A,class B>
typename enable_if_c<
  is_mat<A>::value && is_vec<B>::value && mat_traits<A>::cols==vec_traits<B>::dim, //Condition
  B>::type //Return type
operator*( A const & a, B const & b ){
    B ret;
    ret.a[0] = a.a[0][0] * b.a[0] + a.a[0][1] * b.a[1] + a.a[0][2] * b.a[2];
    ret.a[1] = a.a[1][0] * b.a[0] + a.a[1][1] * b.a[1] + a.a[1][2] * b.a[2];
    ret.a[2] = a.a[2][0] * b.a[0] + a.a[2][1] * b.a[1] + a.a[2][2] * b.a[2];
    //qDebug() << "product";
    return ret;
}
}}


QImage* MainWindow::transformare_proiectiva(QImage* img){
    //using namespace boost::numeric::ublas;
    using namespace boost::qvm;
    QImage* transImage = new QImage(img->width(),img->height(),QImage::Format_RGB32);
    double h4 = 1;
    float3 b, b_s;
    float P[3][3];
    float33 P_inv;
    float3 P4 = {(float)mouse_clicks[3].x, (float)mouse_clicks[3].y, 1.};
    float P_s[3][3];
    float33 P_s_inv;
    float3 P4_s = {0, (float)img->height(), 1};
    float33 A;

    // initial setup
    P[0][0] = mouse_clicks[0].x;
    P[0][1] = mouse_clicks[1].x;
    P[0][2] = mouse_clicks[2].x;
    P[1][0] = mouse_clicks[0].y;
    P[1][1] = mouse_clicks[1].y;
    P[1][2] = mouse_clicks[2].y;
    P[2][0] = 1;
    P[2][1] = 1;
    P[2][2] = 1;

    P_s[0][0] = 0;
    P_s[0][1] = img->width();
    P_s[0][2] = img->width();
    P_s[1][0] = 0;
    P_s[1][1] = 0;
    P_s[1][2] = img->height();
    P_s[2][0] = 1;
    P_s[2][1] = 1;
    P_s[2][2] = 1;

    // calculate b
    // throws delta = 0 error
    auto inv = boost::qvm::inverse(P);
    P_inv = convert_to<float33>(inv);

    auto inv2 = boost::qvm::inverse(P_s);
    P_s_inv = convert_to<float33>(inv2);

    b = P_inv * P4;
    b_s = P_s_inv * P4_s;

    // calculate A
    A.a[0][0] = h4 * ((b_s.a[0]/b.a[0]) * P_s[0][0]);
    A.a[0][1] = h4 * ((b_s.a[1]/b.a[1]) * P_s[0][1]);
    A.a[0][2] = h4 * ((b_s.a[2]/b.a[2]) * P_s[0][2]);

    A.a[1][0] = h4 * ((b_s.a[0]/b.a[0]) * P_s[1][0]);
    A.a[1][1] = h4 * ((b_s.a[1]/b.a[1]) * P_s[1][1]);
    A.a[1][2] = h4 * ((b_s.a[2]/b.a[2]) * P_s[1][2]);

    A.a[2][0] = h4 * ((b_s.a[0]/b.a[0]) * P_s[2][0]);
    A.a[2][1] = h4 * ((b_s.a[1]/b.a[1]) * P_s[2][1]);
    A.a[2][2] = h4 * ((b_s.a[2]/b.a[2]) * P_s[2][2]);

    A = A * P_inv;

//    A.a[0][0] = A.a[0][0] * P_inv.a[0][0] +  A.a[0][1] * P_inv.a[1][0] +  A.a[0][2] * P_inv.a[2][0];
//    A.a[0][1] = A.a[0][0] * P_inv.a[0][1] +  A.a[0][1] * P_inv.a[1][1] +  A.a[0][2] * P_inv.a[2][1];
//    A.a[0][2] = A.a[0][0] * P_inv.a[0][2] +  A.a[0][1] * P_inv.a[1][2] +  A.a[0][2] * P_inv.a[2][2];

//    A.a[1][0] = A.a[1][0] * P_inv.a[0][0] +  A.a[1][1] * P_inv.a[1][0] +  A.a[1][2] * P_inv.a[2][0];
//    A.a[1][1] = A.a[1][0] * P_inv.a[0][1] +  A.a[1][1] * P_inv.a[1][1] +  A.a[1][2] * P_inv.a[2][1];
//    A.a[1][2] = A.a[1][0] * P_inv.a[0][2] +  A.a[1][1] * P_inv.a[1][2] +  A.a[1][2] * P_inv.a[2][2];

//    A.a[2][0] = A.a[2][0] * P_inv.a[0][0] +  A.a[2][1] * P_inv.a[1][0] +  A.a[2][2] * P_inv.a[2][0];
//    A.a[2][1] = A.a[2][0] * P_inv.a[0][1] +  A.a[2][1] * P_inv.a[1][1] +  A.a[2][2] * P_inv.a[2][1];
//    A.a[2][2] = A.a[2][0] * P_inv.a[0][2] +  A.a[2][1] * P_inv.a[1][2] +  A.a[2][2] * P_inv.a[2][2];

    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            transImage->setPixel(i,j, qRgb(0, 0, 0));
        }
    }

    // render img with spaces
//    for (int i = 0; i < img->width(); i++){
//        for (int j = 0; j < img->height();j++){
//            QColor pixel = img->pixelColor(i, j);

//            float3 initialPos;
//            initialPos.a[0] = i;
//            initialPos.a[1] = j;
//            initialPos.a[2] = 1;
//            auto resultPos = A * initialPos;

//            if(resultPos.a[0] / resultPos.a[2] >= 0 && round(resultPos.a[0] / resultPos.a[2]) < img->width() && resultPos.a[1] / resultPos.a[2] >= 0 && round(resultPos.a[1] / resultPos.a[2]) < img->height())
//                transImage->setPixel(round(resultPos.a[0] / resultPos.a[2]),round(resultPos.a[1] / resultPos.a[2]), qRgb(pixel.red(), pixel.green(), pixel.blue()));
//        }
//    }

    // render img with interpolation (billinear)

    auto inv3 = boost::qvm::inverse(A);
    float33 A_inv = convert_to<float33>(inv3);

    for (int i = 0; i < img->width(); i++){
        for (int j = 0; j < img->height();j++){
            float3 initialPos;
            initialPos.a[0] = i;
            initialPos.a[1] = j;
            initialPos.a[2] = 1;

            auto resultPos = A_inv * initialPos;

            //qDebug() << resultPos.a[0] / resultPos.a[2] << " " << resultPos.a[1] / resultPos.a[2];

            double xc = resultPos.a[0] / resultPos.a[2];
            double yc = resultPos.a[1] / resultPos.a[2];
            int x0 = (int)xc;
            int x1 = (int)xc + 1;
            int y0 = (int)yc;
            int y1 = (int)yc + 1;

            if(x1 < img->width() && y1 < img->height() && x0 >=0 && y0 >= 0){
                double fxcy0 = (img->pixelColor(x1, y0).red() - img->pixelColor(x0, y0).red()) * (xc - x0) + img->pixelColor(x0, y0).red();
                double fxcy1 = (img->pixelColor(x1, y1).red() - img->pixelColor(x0, y1).red()) * (xc - x0) + img->pixelColor(x0, y1).red();
                double fxcyc = (fxcy1 - fxcy0) * (yc - y0) + fxcy0;
                transImage->setPixel(i,j,qRgb(fxcyc,fxcyc,fxcyc));
            }
        }
    }

    return transImage;
}
